$svc_conf_template = @(END)
[Unit]
Description=Service for the Investimentos API
 
[Service]
ExecStart=/usr/bin/java -javaagent:/home/ubuntu/appagent/javaagent.jar -jar /home/ubuntu/Api-Investimentos-0.0.1-SNAPSHOT.jar

[Install]
WantedBy=multi-user.target
END

class { 'java':
   package => 'openjdk-8-jdk',
}
ssh_authorized_key {'jenkins@banana':
   ensure => present,
   key => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQDvNfe5q8SfXeGyl6cB2vR4mo7pHq30sjPgMrhP05UF2gH3bgz7dGREpb6caCGJo8gpQXofeEI0wdqLw0KbmIuqSMpT4yQQHWr6VqWbDaK2XQutHJkElGQMTCBADwT4bGt8b2eNV8s581g/RS01q4L7F9iSkMzSms3BM63FkZHZAOlOTHmRnN3Ju39MolmjGPwb6SpUFYpMIZLic1vKTe4p1L3PQL4czIJxpssE283iOeRiAceD/Xaly4mgVqdxmMEm3gZo5PzcPMu5Y08M6Ur0vJR5i4IVSSheBhzcKLshuvTBt5bobYtG1BWatQIpbKoCO6Uqnf/7U7FvPHJTYsJb',
   type => 'ssh-rsa',
   user => 'ubuntu',
}
ssh_authorized_key {'a2@localhost.localdomain':
   ensure => present,
   key => 'AAAAB3NzaC1yc2EAAAADAQABAAABgQDbTcG7qp7+tBSQ8wvCKw2xVN9iEoj/WUF7Q12PfpH6zzau1PKDy0S548nWz61au7smkyBKBxokH/Zuneesm4FKxG/6aqTXz3fcHoQaMnouCAqTOGGSoM8fiBQrh8ZaG6DQiM0BLFpeHzxxSq+kxxrmbM1UakLhfAsHBLCsJXfymwOvAalPYQu/tmhnotlf/wkHHi3O/iBpystU7aanSLVwZ9YpBquXpMSLe83Yu++xyO0I6ezAuoFOyJoN8Xvru2CpXnoecECdbVDYdUhux9peNBjoSMn2Zq92A0KCWhzR0hltKDFjia5Du3+8927EACjBZdS2CwHUIgvNmP4AthfW8TOmJr/0vA8Mnp1RRM00QGRBbxj2X5praLb7g6gqP4m4dLljzGkGZB48UcKjWW3L71ENBEHxVS9KikKEEFZaCWDvdSYDeyKptXmf0ZE6/6TDx5Aw/8DhgWiyoN8l/JoLJNvXZ+QChHfEvxIBhGaSJKZaFYFxnZTCnEV7/7ol8xc=',
   type => 'ssh-rsa',
   user => 'ubuntu',
} 

file { '/etc/systemd/system/api-invest.service':
   ensure => file,
   content => inline_epp($svc_conf_template),
}
service { 'api-invest':
   ensure => running,
}
class{"nginx": }
   nginx::resource::server { 'meuapp':
   server_name => ['_'],
   listen_port => 80,
   proxy => 'http://localhost:8080',
}
file { "/etc/nginx/conf.d/default.conf":
   ensure => absent,
   notify => Service['nginx'],
}

